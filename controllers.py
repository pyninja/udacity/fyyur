from app import app
from models import City, Venue, Artist, Show, db, State, Genre
from flask import flash, redirect, url_for
from flask import render_template, request
from sqlalchemy.sql.expression import and_
import datetime as dt
from forms import *
from sqlalchemy.exc import SQLAlchemyError


# ---------------------------------------------------------------------------#
# Controllers.
# ---------------------------------------------------------------------------#
@app.route('/')
def index():
    return render_template('pages/home.html')


#  Venues
#  ----------------------------------------------------------------

@app.route('/venues')
def venues():
    # TODO: replace with real venues data.
    # num_shows should be aggregated based on number of upcoming shows per venue.
    data = []
    cities = City.query.order_by(City.city.desc()).all()
    for item in cities:
        response = dict()
        venue_list = Venue.query.filter_by(city_id=item.id).all()
        if len(venue_list) > 0:
            venue_info = []
            # using backref to get the state
            response["state"] = item.state_.state
            response["city"] = item.city
            # number of upcoming shows
            for venue in venue_list:
                show_count = (
                  Show.query.filter(
                    and_(Show.start_time >= dt.datetime.now(), Show.venue_id == venue.id)
                  ).count()
                )
                venue_info.append({"id": venue.id, "name": venue.name, "num_upcoming_shows": show_count})
                response["venues"] = venue_info
            data.append(response)
    return render_template('pages/venues.html', areas=data)


@app.route('/venues/search', methods=['POST'])
def search_venues():
    response = dict()
    search_str = request.form.get('search_term', '')
    venue_list = Venue.query.filter(Venue.name.ilike(r"%{}%".format(search_str))).all()
    response["count"] = len(venue_list)
    response["data"] = []
    if response.get("count") > 0:
        for venue in venue_list:
            show_count = (
                Show.query.filter(
                    and_(Show.start_time >= dt.datetime.now(), Show.venue_id == venue.id)
                ).count()
            )
            response["data"].append({"id": venue.id, "name": venue.name, "num_upcoming_shows": show_count})
    return render_template('pages/search_venues.html', results=response, search_term=request.form.get('search_term', ''))


@app.route('/venues/<int:venue_id>')
def show_venue(venue_id):
    data = {}
    venue = Venue.query.get(venue_id)
    if venue:
        city_obj = City.query.get(venue.city_id)
        past_show_list = Show.query.filter(and_(Show.start_time <= dt.datetime.now(), Show.venue_id == venue.id)).all()
        next_show_list = Show.query.filter(and_(Show.start_time >= dt.datetime.now(), Show.venue_id == venue.id)).all()
        past_shows = []
        next_shows = []

        if len(past_show_list) > 0:
            for item in past_show_list:
                artist = item.artist_
                past_shows.append(
                    {"artist_id": artist.id,
                     "artist_name": artist.name,
                     "artist_image_link": artist.image_link,
                     "start_time": item.start_time.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
                     }
                )

        if len(next_show_list) > 0:
            for item in next_show_list:
                artist = item.artist_
                next_shows.append(
                    {"artist_id": artist.id,
                     "artist_name": artist.name,
                     "artist_image_link": artist.image_link,
                     "start_time": item.start_time.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
                     }
                )

        data.update({
            'id': venue.id,
            'name': venue.name,
            'address': venue.address,
            'phone': venue.phone,
            'website': venue.website,
            'image_link': venue.image_link,
            'facebook_link': venue.facebook_link,
            'seeking_talent': venue.seeking_talent,
            'seeking_description': venue.seeking_description,
            'genres': [item.genre for item in venue.genres],
            'state': city_obj.state_.state,
            'city': city_obj.city,
            'past_shows': past_shows,
            'past_shows_count': len(past_shows),
            'upcoming_shows': next_shows,
            'upcoming_shows_count': len(next_shows)
        }
        )
    return render_template('pages/show_venue.html', venue=data)


#  Create Venue
#  ----------------------------------------------------------------
@app.route('/venues/create', methods=['GET'])
def create_venue_form():
    form = VenueForm()
    return render_template('forms/new_venue.html', form=form)


@app.route('/venues/create', methods=['POST'])
def create_venue_submission():
    name = request.form["name"]
    city = request.form["city"]
    state = request.form["state"]
    phone = request.form["phone"]
    address = request.form["address"]
    website = request.form.get("website")
    image_link = request.form.get("image_link")
    facebook_link = request.form["facebook_link"]
    genres = request.form.getlist("genres")
    try:
        venue = Venue(
            name=name, phone=phone, address=address,
            website=website, facebook_link=facebook_link,
            image_link=image_link
        )
        state_obj = State.query.filter_by(state=state).first()
        city_obj = City.query.filter_by(city=city).first()

        if state_obj:
            if city_obj:
                venue.city_id = city_obj.id
            else:
                city_obj = City(city=city)
                city_obj.state_id = state_obj.id
                venue.city_ = city_obj
        else:
            state_obj = State(state=state)
            city_obj = City(city=city)
            venue.city_ = city_obj
            city_obj.state_ = state_obj

        for idx in genres:
            genre = Genre.query.filter_by(genre=idx).first()
            if genre:
                venue.genres.append(genre)
            else:
                genre = Genre(genre=idx)
                venue.genres.append(genre)
        db.session.add(venue)
        db.session.commit()
        flash('Venue ' + name + ' was listed!')
        return render_template('pages/home.html')
    except SQLAlchemyError as e:
        print(e)
        db.session.rollback()
        flash('Venue ' + request.form['name'] + ' was not listed!')
        return render_template('errors/404.html')
    finally:
        db.session.close()


@app.route('/venues/<venue_id>', methods=['DELETE'])
def delete_venue(venue_id):
    venue_obj = Venue.query.get(venue_id)
    try:
        for genres in venue_obj.genres:
            db.session.delete(genres)
        db.session.delete(venue_obj)
        db.session.comit()
    except SQLAlchemyError as e:
        print(e)
    # BONUS CHALLENGE: Implement a button to delete a Venue on a Venue Page, have it so that
    # clicking that button delete it from the db then redirect the user to the homepage
    return None


#  Artists
#  ----------------------------------------------------------------
@app.route('/artists')
def artists():
    artist = Artist.query.all()
    data = []
    if len(artist) > 0:
        for item in artist:
            data.append({"id": item.id, "name": item.name})
    return render_template('pages/artists.html', artists=data)


@app.route('/artists/search', methods=['POST'])
def search_artists():
    search_str = request.form.get('search_term', '')
    response = dict()
    artist_list = Artist.query.filter(Artist.name.ilike(r"%{}%".format(search_str))).all()
    response["count"] = len(artist_list)
    response["data"] = []
    if response["count"] > 0:
        for item in artist_list:
            show_count = (
                Show.query.filter(
                    and_(Show.start_time >= dt.datetime.now(), Show.artist_id == item.id)
                ).count()
            )
            response["data"].append({"id": item.id, "name": item.name, "num_upcoming_shows": show_count})
    return render_template('pages/search_artists.html', results=response, search_term=request.form.get('search_term', ''))


@app.route('/artists/<int:artist_id>')
def show_artist(artist_id):
    data = {}
    artist = Artist.query.get(artist_id)
    if artist:
        city_obj = City.query.get(artist.city_id)
        past_show_list = Show.query.filter(and_(Show.start_time <= dt.datetime.now(), Show.artist_id == artist.id)).all()
        next_show_list = Show.query.filter(and_(Show.start_time >= dt.datetime.now(), Show.artist_id == artist.id)).all()
        past_shows = []
        next_shows = []

        if len(past_show_list) > 0:
            for item in past_show_list:
                venue = item.venue_
                past_shows.append(
                    {"venue_id": venue.id,
                     "venue_name": venue.name,
                     "venue_image_link": venue.image_link,
                     "start_time": item.start_time.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
                     }
                )
        if len(next_show_list) > 0:
            for item in next_show_list:
                venue = item.venue_
                next_shows.append(
                    {"venue_id": venue.id,
                     "venue_name": venue.name,
                     "venue_image_link": venue.image_link,
                     "start_time": item.start_time.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
                     }
                )
        data.update({
            "id": artist.id,
            "name": artist.name,
            "genres": [item.genre for item in artist.genres],
            "city": city_obj.city,
            "state": city_obj.state_.state,
            "phone": artist.phone,
            "facebook_link": artist.facebook_link,
            "seeking_venue": artist.seeking_venue,
            "image_link": artist.image_link,
            "past_shows": past_shows,
            "upcoming_shows": next_shows,
            "past_shows_count": len(past_show_list),
            "upcoming_shows_count": len(next_show_list)
        })
    return render_template('pages/show_artist.html', artist=data)


#  Update
#  ----------------------------------------------------------------
@app.route('/artists/<int:artist_id>/edit', methods=['GET'])
def edit_artist(artist_id):
    form = ArtistForm()
    artist_obj = Artist.query.get(artist_id)
    city_obj = City.query.get(artist_obj.city_id)

    artist = dict()
    if artist_obj:
        artist.update({
            "id": artist_obj.id,
            "name": artist_obj.name,
            "genres": [item.genre for item in artist_obj.genres],
            "city": city_obj.city,
            "state": city_obj.state_.state,
            "phone": artist_obj.phone,
            "facebook_link": artist_obj.facebook_link,
            "seeking_venue": artist_obj.seeking_venue,
            "seeking_description": artist_obj.seeking_description,
            "image_link": artist_obj.image_link
        })
    # TODO: populate form with fields from artist with ID <artist_id>
    return render_template('forms/edit_artist.html', form=form, artist=artist)


@app.route('/artists/<int:artist_id>/edit', methods=['POST'])
def edit_artist_submission(artist_id):
    city = request.form["city"]
    state = request.form["state"]
    artist_obj = Artist.query.get(artist_id)

    # Need to detect if the city and state is already in the database or not
    city_obj = City.query.filter_by(city=city).first()
    state_obj = State.query.filter_by(state=state).first()

    if city_obj:
        artist_obj.city_id = city_obj.id
    elif state_obj:
        city_obj = City(city=city)
        city_obj.state_id = state_obj.id
    else:
        state_obj = State(state=state)
        city_obj = City(city=city)
        artist_obj.city_ = city_obj
        city_obj.state_ = state_obj
    try:
        artist_obj.name = request.form["name"]
        artist_obj.phone = request.form["phone"]
        artist_obj.website = request.form.get("website")
        artist_obj.image_link = request.form.get("image_link")
        artist_obj.facebook_link = request.form["facebook_link"]
        artist_obj.genres = []
        for idx in request.form.getlist("genres"):
            genre = Genre.query.filter_by(genre=idx).first()
            if genre:
                artist_obj.genres.append(genre)
            else:
                genre = Genre(genre=idx)
                artist_obj.genres.append(genre)
        db.session.commit()
        flash("Artist info. edited.")
        return redirect(url_for('show_artist', artist_id=artist_id))
    except SQLAlchemyError as e:
        print(e)
        db.session.rollback()
        flash("Sorry, Can not Edit.")
        return redirect(url_for('show_artist', artist_id=artist_id))
    finally:
        db.session.close()


@app.route('/venues/<int:venue_id>/edit', methods=['GET'])
def edit_venue(venue_id):
    form = VenueForm()
    venue_obj = Venue.query.get(venue_id)
    city_obj = City.query.get(venue_obj.city_id)
    venue = dict()
    if venue_obj:
        venue.update({
            "id": venue_obj.id,
            "name": venue_obj.name,
            "genres": [item.genre for item in venue_obj.genres],
            "address": venue_obj.address,
            "city": city_obj.city,
            "state": city_obj.state_.state,
            "phone": venue_obj.phone,
            "website": venue_obj.website,
            "facebook_link": venue_obj.facebook_link,
            "seeking_talent": venue_obj.seeking_talent,
            "seeking_description": venue_obj.seeking_description,
            "image_link": venue_obj.image_link
        })
    return render_template('forms/edit_venue.html', form=form, venue=venue)


@app.route('/venues/<int:venue_id>/edit', methods=['POST'])
def edit_venue_submission(venue_id):
    # form = VenueForm(request.form)
    # TODO: take values from the form submitted, and update existing
    city = request.form["city"]
    state = request.form["state"]
    venue_obj = Venue.query.get(venue_id)
    city_obj = City.query.filter_by(city=city).first()
    state_obj = State.query.filter_by(state=state).first()

    # Need to detect if the city and state is already in the database or not
    if city_obj:
        venue_obj.city_id = city_obj.id
    elif state_obj:
        city_obj = City(city=city)
        city_obj.state_id = state_obj.id
    else:
        state_obj = State(state=state)
        city_obj = City(city=city)
        venue_obj.city_ = city_obj
        city_obj.state_ = state_obj
    try:
        venue_obj.name = request.form["name"]
        venue_obj.phone = request.form["phone"]
        venue_obj.website = request.form.get("website")
        venue_obj.image_link = request.form.get("image_link")
        venue_obj.facebook_link = request.form["facebook_link"]
        venue_obj.address = request.form["address"]
        venue_obj.genres = []
        for idx in request.form.getlist("genres"):
            genre = Genre.query.filter_by(genre=idx).first()
            if genre:
                venue_obj.genres.append(genre)
            else:
                genre = Genre(genre=idx)
                venue_obj.genres.append(genre)
        db.session.commit()
        flash("Venue info. edited.")
        return redirect(url_for('show_venue', venue_id=venue_id))
    except SQLAlchemyError as e:
        print(e)
        db.session.rollback()
        flash("Sorry, Can not Edit.")
        return redirect(url_for('show_venue', venue_id=venue_id))
    finally:
        db.session.close()


#  Create Artist
#  ----------------------------------------------------------------
@app.route('/artists/create', methods=['GET'])
def create_artist_form():
    form = ArtistForm()
    return render_template('forms/new_artist.html', form=form)


@app.route('/artists/create', methods=['POST'])
def create_artist_submission():
    name = request.form["name"]
    city = request.form["city"]
    state = request.form["state"]
    phone = request.form["phone"]
    website = request.form.get("website")
    image_link = request.form.get("image_link")
    facebook_link = request.form["facebook_link"]
    genres = request.form.getlist("genres")
    try:
        artist = Artist(name=name, phone=phone, website=website, facebook_link=facebook_link, image_link=image_link)
        state_obj = State.query.filter_by(state=state).first()
        city_obj = City.query.filter_by(city=city).first()

        if state_obj:
            if city_obj:
                artist.city_id = city_obj.id
            else:
                city_obj = City(city=city)
                city_obj.state_id = state_obj.id
                artist.city_ = city_obj
        else:
            state_obj = State(state=state)
            city_obj = City(city=city)
            artist.city_ = city_obj
            city_obj.state_ = state_obj

        for idx in genres:
            genre = Genre.query.filter_by(genre=idx).first()
            if genre:
                artist.genres.append(genre)
            else:
                genre = Genre(genre=idx)
                artist.genres.append(genre)
        db.session.add(artist)
        db.session.commit()
        flash('Artist ' + name + ' was successfully listed!')
        return render_template('pages/home.html')
    except SQLAlchemyError as e:
        print(e)
        db.session.rollback()
        flash('Show was not listed!')
        return render_template('errors/404.html')
    finally:
        db.session.close()


#  Shows
#  ----------------------------------------------------------------
@app.route('/shows')
def shows():
    # displays list of shows at /shows
    # TODO: replace with real venues data.
    #       num_shows should be aggregated based on number of upcoming shows per venue.
    data = []
    show_list = Show.query.all()
    for item in show_list:
        artist_subquery = item.artist_
        venue_subquery = item.venue_
        data.append({
            "venue_id": item.venue_id,
            "venue_name": venue_subquery.name,
            "artist_id": item.artist_id,
            "artist_name": artist_subquery.name,
            "artist_image_link": artist_subquery.image_link,
            "start_time": item.start_time.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
        })
    return render_template('pages/shows.html', shows=data)


@app.route('/shows/create')
def create_shows():
    # renders form. do not touch.
    form = ShowForm()
    return render_template('forms/new_show.html', form=form)


@app.route('/shows/create', methods=['POST'])
def create_show_submission():
    try:
        artist_id = request.form["artist_id"]
        venue_id = request.form["venue_id"]
        start_time = datetime.strptime(request.form["start_time"], '%Y-%m-%d %H:%M:%S')
        show = Show(
            venue_id=venue_id,
            artist_id=artist_id,
            start_time=start_time,
            end_time=start_time + dt.timedelta(hours=60)
        )
        db.session.add(show)
        db.session.commit()
        # on successful db insert, flash success
        flash('Show was successfully listed!')
        return render_template('pages/home.html')
    except SQLAlchemyError as e:
        print(e)
        db.session.rollback()
        flash('Show was not listed!')
        return render_template('errors/404.html')
    finally:
        db.session.close()


@app.errorhandler(404)
def not_found_error(error):
    return render_template('errors/404.html'), 404


@app.errorhandler(500)
def server_error(error):
    return render_template('errors/500.html'), 500
