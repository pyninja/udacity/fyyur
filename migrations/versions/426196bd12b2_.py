"""empty message

Revision ID: 426196bd12b2
Revises: 87c0dfa81bd0
Create Date: 2020-01-12 12:08:35.856332

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '426196bd12b2'
down_revision = '87c0dfa81bd0'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('Genre',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('genre', sa.String(length=120), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('genre')
    )
    op.create_table('Artist_Genres',
    sa.Column('artist_id', sa.Integer(), nullable=False),
    sa.Column('genre_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['artist_id'], ['Artist.id'], ),
    sa.ForeignKeyConstraint(['genre_id'], ['Genre.id'], ),
    sa.PrimaryKeyConstraint('artist_id', 'genre_id')
    )
    op.create_table('Venue_Genres',
    sa.Column('venue_id', sa.Integer(), nullable=False),
    sa.Column('genre_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['genre_id'], ['Genre.id'], ),
    sa.ForeignKeyConstraint(['venue_id'], ['Venue.id'], ),
    sa.PrimaryKeyConstraint('venue_id', 'genre_id')
    )
    op.drop_table('Venue_Geners')
    op.drop_table('Artist_Geners')
    op.drop_table('Gener')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('Artist_Geners',
    sa.Column('artist_id', sa.INTEGER(), autoincrement=False, nullable=False),
    sa.Column('gener_id', sa.INTEGER(), autoincrement=False, nullable=False),
    sa.ForeignKeyConstraint(['artist_id'], ['Artist.id'], name='Artist_Geners_artist_id_fkey'),
    sa.ForeignKeyConstraint(['gener_id'], ['Gener.id'], name='Artist_Geners_gener_id_fkey'),
    sa.PrimaryKeyConstraint('artist_id', 'gener_id', name='Artist_Geners_pkey')
    )
    op.create_table('Gener',
    sa.Column('id', sa.INTEGER(), server_default=sa.text('nextval(\'"Gener_id_seq"\'::regclass)'), autoincrement=True, nullable=False),
    sa.Column('gener', sa.VARCHAR(length=120), autoincrement=False, nullable=False),
    sa.PrimaryKeyConstraint('id', name='Gener_pkey'),
    sa.UniqueConstraint('gener', name='Gener_gener_key'),
    postgresql_ignore_search_path=False
    )
    op.create_table('Venue_Geners',
    sa.Column('venue_id', sa.INTEGER(), autoincrement=False, nullable=False),
    sa.Column('gener_id', sa.INTEGER(), autoincrement=False, nullable=False),
    sa.ForeignKeyConstraint(['gener_id'], ['Gener.id'], name='Venue_Geners_gener_id_fkey'),
    sa.ForeignKeyConstraint(['venue_id'], ['Venue.id'], name='Venue_Geners_venue_id_fkey'),
    sa.PrimaryKeyConstraint('venue_id', 'gener_id', name='Venue_Geners_pkey')
    )
    op.drop_table('Venue_Genres')
    op.drop_table('Artist_Genres')
    op.drop_table('Genre')
    # ### end Alembic commands ###
