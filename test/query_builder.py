from app import State, City, Venue, Artist, Genre, Show
from datetime import datetime
from sqlalchemy.sql.expression import and_
from pprint import pprint

# Venues
data = []
cities = City.query.order_by(City.city.desc()).all()
for item in cities:
    response = dict()
    venue_list = Venue.query.filter_by(city_id=item.id).all()
    if len(venue_list) > 0:
        venue_info = []
        response["state"] = item.state_.state
        response["city"] = item.city
        for venue in venue_list:
            show_count = (
                Show.query.filter(
                    and_(Show.start_time >= datetime.now(), Show.venue_id == venue.id)
                ).count()
            )
            venue_info.append({"id": venue.id, "name": venue.name, "num_upcoming_shows": show_count})
            response["venues"] = venue_info
        data.append(response)

pprint(data)

# Venues/search
search_item = "Music"
response = dict()
venue_list = Venue.query.filter(Venue.name.ilike(r"%{}%".format("Music"))).all()
response["count"] = len(venue_list)
response["data"] = []
if response.get("count") > 0:
    for venue in venue_list:
        show_count = (
            Show.query.filter(
                and_(Show.start_time >= datetime.now(), Show.venue_id == venue.id)
            ).count()
        )
        response["data"].append({"id": venue.id, "name": venue.name, "num_upcoming_shows": show_count})

pprint(response)


# venues/venue_id
venue_id = 1
data = {}
venue = Venue.query.get(venue_id)
if venue:
    city_obj = City.query.get(venue.city_id)
    past_show_list = Show.query.filter(and_(Show.start_time <= datetime.now(), Show.venue_id == venue.id)).all()
    next_show_list = Show.query.filter(and_(Show.start_time >= datetime.now(), Show.venue_id == venue.id)).all()
    past_shows = []
    next_shows = []

    if len(past_show_list) > 0:
        for item in past_show_list:
            artist = item.artist_
            past_shows.append(
                {"artist_id": artist.id,
                 "artist_name": artist.name,
                 "artist_image_link": artist.image_link,
                 "start_time": item.start_time.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
                 }
            )

    if len(next_show_list) > 0:
        for item in next_show_list:
            artist = item.artist_
            next_shows.append(
                {"artist_id": artist.id,
                 "artist_name": artist.name,
                 "artist_image_link": artist.image_link,
                 "start_time": item.start_time.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
                 }
            )

    data.update({
        'id': venue.id,
        'name': venue.name,
        'address': venue.address,
        'phone': venue.phone,
        'website': venue.website,
        'image_link': venue.image_link,
        'facebook_link': venue.facebook_link,
        'seeking_talent': venue.seeking_talent,
        'seeking_description': venue.seeking_description,
        'genres': [item.genre for item in venue.genres],
        'state': city_obj.state_.state,
        'city': city_obj.city,
        'past_shows': past_shows,
        'past_shows_count': len(past_shows),
        'upcoming_shows': next_shows,
        'upcoming_shows_count': len(next_shows)
    }
    )


# artists/
artist = Artist.query.all()
data = []
if len(artist) > 0:
    for item in artist:
        data.append({"id": item.id, "name": item.name})

# artist/search
search_str = 'Guns'
response = dict()
artist_list = Artist.query.filter(Artist.name.ilike(r"%{}%".format(search_str))).all()
response["count"] = len(artist_list)
response["data"] = []
if response["count"] > 0:
    for item in artist_list:
        show_count = (
            Show.query.filter(
                and_(Show.start_time >= datetime.now(), Show.artist_id == item.id)
            ).count()
        )
        response["data"].append({"id": item.id, "name": item.name, "num_upcoming_shows": show_count})

# artist/artist_id
artist_id = 4
data = {}
artist = Artist.query.get(artist_id)
if artist:
    city_obj = City.query.get(artist.city_id)
    past_show_list = Show.query.filter(and_(Show.start_time <= datetime.now(), Show.artist_id == artist.id)).all()
    next_show_list = Show.query.filter(and_(Show.start_time >= datetime.now(), Show.artist_id == artist.id)).all()
    past_shows = []
    next_shows = []

    if len(past_show_list) > 0:
        for item in past_show_list:
            venue = item.venue_
            past_shows.append(
                {"venue_id": venue.id,
                 "venue_name": venue.name,
                 "venue_image_link": venue.image_link,
                 "start_time": item.start_time.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
                 }
            )
    if len(next_show_list) > 0:
        for item in next_show_list:
            venue = item.venue_
            next_shows.append(
                {"venue_id": venue.id,
                 "venue_name": venue.name,
                 "venue_image_link": venue.image_link,
                 "start_time": item.start_time.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
                 }
            )
    data.update({
        "id": artist.id,
        "name": artist.name,
        "genres": [item.genre for item in artist.genres],
        "city": city_obj.city,
        "state": city_obj.state_.state,
        "phone": artist.phone,
        "facebook_link": artist.facebook_link,
        "seeking_venue": artist.seeking_venue,
        "image_link": artist.image_link,
        "past_shows": past_shows,
        "upcoming_shows": next_shows,
        "past_shows_count": len(past_show_list),
        "upcoming_shows_count": len(next_show_list)
    })

pprint(data)

# '/artists/<int:artist_id>/edit'
artist_id = 4
artist_obj = Artist.query.get(artist_id)
city_obj = City.query.get(artist_obj.city_id)

artist = dict()
if artist_obj:
    artist.update({
        "id": artist_obj.id,
        "name": artist_obj.name,
        "genres": [item.genre for item in artist_obj.genres],
        "city": city_obj.city,
        "state": city_obj.state_.state,
        "phone": artist_obj.phone,
        "facebook_link": artist_obj.facebook_link,
        "seeking_venue": artist_obj.seeking_venue,
        "seeking_description": artist_obj.seeking_description,
        "image_link": artist_obj.image_link,
    })

# venue/venue_id/edit
venue_id = 1
venue_obj = Venue.query.get(1)
city_obj = City.query.get(venue_obj.city_id)
venue = dict()
if venue_obj:
    venue.update({
        "id": venue_obj.id,
        "name": venue_obj.name,
        "genres": [item.genre for item in venue_obj.genres],
        "address": venue_obj.address,
        "city": city_obj.city,
        "state": city_obj.state_.state,
        "phone": venue_obj.phone,
        "website": venue_obj.website,
        "facebook_link": venue_obj.facebook_link,
        "seeking_talent": venue_obj.seeking_talent,
        "seeking_description": venue_obj.seeking_description,
        "image_link": venue_obj.image_link
    })

# shows/
data = []
show_list = Show.query.all()
for item in show_list:
    artist_subquery = item.artist_
    venue_subquery = item.venue_
    data.append({
        "venue_id": item.venue_id,
        "venue_name": venue_subquery.name,
        "artist_id": item.artist_id,
        "artist_name": artist_subquery.name,
        "artist_image_link": artist_subquery.image_link,
        "start_time": item.start_time.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
    }
    )

pprint(data)



